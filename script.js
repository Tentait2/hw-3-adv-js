"use strict"

// Поясніть своїми словами, як ви розумієте, що таке деструктуризація і навіщо вона потрібна

// Безумно удобная возможность доставать свойства из объектов или массивов

function firstTask(){
    const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
    const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

    console.log([...new Set([...clients1,...clients2])])
}
firstTask()

function secondTask(){
    const characters = [
        {
            name: "Елена",
            lastName: "Гилберт",
            age: 17,
            gender: "woman",
            status: "human"
        },
        {
            name: "Кэролайн",
            lastName: "Форбс",
            age: 17,
            gender: "woman",
            status: "human"
        },
        {
            name: "Аларик",
            lastName: "Зальцман",
            age: 31,
            gender: "man",
            status: "human"
        },
        {
            name: "Дэймон",
            lastName: "Сальваторе",
            age: 156,
            gender: "man",
            status: "vampire"
        },
        {
            name: "Ребекка",
            lastName: "Майклсон",
            age: 1089,
            gender: "woman",
            status: "vempire"
        },
        {
            name: "Клаус",
            lastName: "Майклсон",
            age: 1093,
            gender: "man",
            status: "vampire"
        }
    ];
    let charactersShortInfo = []
    characters.forEach((el) =>{
        let {name,lastName, age} = el
        charactersShortInfo.push({name : name, lastName : lastName, age : age})
    })
    console.log(charactersShortInfo)
}
secondTask()

function thirdTask (){
    const user1 = {
        name: "John",
        years: 30
    };
      let {name, years, isAdmin = false} = user1;

      console.log(name , years, isAdmin);

}
thirdTask()

function fourthTask(){
    const satoshi2020 = {
        name: 'Nick',
        surname: 'Sabo',
        age: 51,
        country: 'Japan',
        birth: '1979-08-21',
        location: {
            lat: 38.869422,
            lng: 139.876632
        }
    }

    const satoshi2019 = {
        name: 'Dorian',
        surname: 'Nakamoto',
        age: 44,
        hidden: true,
        country: 'USA',
        wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
        browser: 'Chrome'
    }

    const satoshi2018 = {
        name: 'Satoshi',
        surname: 'Nakamoto',
        technology: 'Bitcoin',
        country: 'Japan',
        browser: 'Tor',
        birth: '1975-04-05'
    }
    let fullProfile = {...satoshi2018,...satoshi2019,...satoshi2020}
    console.log(fullProfile)
}

fourthTask()


function fifthTask(){
    const books = [{
        name: 'Harry Potter',
        author: 'J.K. Rowling'
    }, {
        name: 'Lord of the rings',
        author: 'J.R.R. Tolkien'
    }, {
        name: 'The witcher',
        author: 'Andrzej Sapkowski'
    }];

    const bookToAdd = {
        name: 'Game of thrones',
        author: 'George R. R. Martin'
    }
    let newArrayBooks = [...books, bookToAdd]
    console.log(newArrayBooks)
}

fifthTask()

function sixthTask(){
    const employee = {
        name: 'Vitalii',
        surname: 'Klichko'
    }

    let newObj = {...employee, age: 18, salary:2000}
    console.log(newObj)
}
sixthTask()


function seventhTask(){
    const array = ['value', () => 'showValue'];

    let [value, showValue] = array;

    alert(value); // має бути виведено 'value'
    alert(showValue());  // має бути виведено 'showValue'
}
seventhTask()
